
import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.Poligonal;
import utfpr.ct.dainf.if62c.projeto.Ponto;
import utfpr.ct.dainf.if62c.projeto.PontoXY;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;
import utfpr.ct.dainf.if62c.projeto.PontoYZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    
    
    public static void main(String[] args)  {

        //PontoYZ teste = new PontoYZ(0.6462361767464113,2.0372444660108764);
        //String valor_p = teste.toString();
        //System.out.println(valor_p);
        try {
        PontoXZ[] nos= new PontoXZ[3];
        
        PontoXZ no0 = new PontoXZ(-3.0,2.0);
        PontoXZ no1 = new PontoXZ(-3.0,6.0);
        PontoXZ no2 = new PontoXZ(0.0,2.0);
        //PontoXZ no3 = new PontoXZ(8.0,3.0);
        
                
        nos[0] = no0;
        nos[1] = no1;
        nos[2] = no2;
        //nos[3] = no3;
        PoligonalFechada vertice = new PoligonalFechada(nos);
        //double valor = nos[0].getX();
        //System.out.println("Valor X = "+valor+"\n");
        vertice.set(0,no0);
        vertice.set(1,no1);
        vertice.set(2,no2);
        //vertice.set(3,no3);
        //System.out.printf("Quantidade de Vertices = %d\n",vertice.getN());
        double resultado = vertice.getComprimento();
        String resultadof = String.format(""+resultado).replace(".", ",");
        //PontoXZ vert = vertice.get(0);
        //String x1 = vert.toString();
        System.out.println("Comprimento da Poligonal = "+resultadof);
/*        Ponto busca = new Ponto(1.0,2.0,3.0);   // Override toString
        String saida = busca.toString();
        System.out.println(saida+"\n"); */
        } //finally {}
        catch (RuntimeException rex) { System.out.println(rex.getLocalizedMessage()); }
        
/*        PontoXY valor = new PontoXY(1.0,2.0);    // Cálculo com 2 coordenadas
        PontoXY valor1 = new PontoXY(4.0,6.0);
        double retorno = valor.dist(valor1);
        System.out.println("Distancia = "+retorno); */
        
        
/*        Ponto busca = new Ponto(1.0,2.0,3.0); // Calcula Distancia entre pontos
        Ponto outro = new Ponto(3.0,4.0,4.0);
        double retorno = busca.dist(outro);*/
        //boolean valor = outro.equals(busca);     // Override equals
        //if (valor == true) System.out.println("Verdadeiro\n"); else System.out.println("Falso\n");
        //System.out.println("Distancia = "+retorno);
    }
    
}
