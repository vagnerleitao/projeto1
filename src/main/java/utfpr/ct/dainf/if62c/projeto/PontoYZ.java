/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author vagner
 */
public class PontoYZ extends Ponto2D {
    private double x, y, z;
    
    public PontoYZ(){
        setX(0.0);
        setY(0.0);
        setZ(0.0);
    }
    
    public PontoYZ(double y, double z){
        setX(0.0);
        setY(y);
        setZ(z);
    }
    
    @Override
    public String toString(){
        String valor = String.format(
                getNome()+"("+
                        String.format(""+getY()).replace(".", ",")+","+
                        String.format(""+getZ()).replace(".", ",")+")"
        );
        return valor;
    }    
    
}
