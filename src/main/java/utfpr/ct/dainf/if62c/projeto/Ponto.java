package utfpr.ct.dainf.if62c.projeto;



/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    public Ponto(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    public Ponto(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
        //String valorp = toString();
        //System.out.println(valorp);
    }
    
    public double getX(){
        return this.x;
    }
    
    public double getY(){
        return this.y;
    }
    
    public double getZ(){
        return this.z;
    }
    
    public void setX(double x){
        this.x = x;
    }
    
    public void setY(double y){
        this.y = y;
    }    
    
    public void setZ(double z){
        this.z = z;
    }
    
    @Override
    public String toString(){
        //String xx = String.format(""+getX()).replace(".", ",");
        String valor = String.format(
                getNome()+"("+
                        String.format(""+getX()).replace(".", ",")+","+
                        String.format(""+getY()).replace(".", ",")+","+
                        String.format(""+getZ()).replace(".", ",")+")"
        );
        return valor;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null || (this.getClass() != o.getClass())){
           return false;
        }
        Ponto valor = (Ponto) o;
        double x1 = this.x;
        double y1 = this.y;
        double z1 = this.z;
        //System.out.println("\nx = "+valor.x+" y = "+valor.y+" z = "+valor.z+"\n x1 = "+x1+" y1 = "+y1+" z1 = "+z+"\n");
        if( x1 != valor.x || y1 != valor.y || z1 != valor.z ) return false;
        else return true;
    }
    
    public double dist(Ponto p){
        Ponto valor = (Ponto) p;
        double resultado;
        //System.out.println("\nx = "+valor.getX()+" y = "+valor.getY()+" z = "+valor.getZ()+
        //        "\nx1 = "+this.x+" y1 = "+this.y+" z1 = "+this.z+"\n");
        resultado = Math.sqrt(Math.pow(this.x-valor.getX(),2)+Math.pow(this.y-valor.getY(),2)+Math.pow(this.z-valor.getZ(), 2));
        return resultado;
    }
    
    
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

}
