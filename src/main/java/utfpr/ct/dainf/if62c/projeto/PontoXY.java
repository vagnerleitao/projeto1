/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author vagner
 */
public class PontoXY extends Ponto2D {
    private double x, y, z;
    
    public PontoXY(){
        setX(0.0);
        setY(0.0);
        setZ(0.0);
    }
    
    public PontoXY(double x, double y){
        setX(x);
        setY(y);
        setZ(0.0);
        //this.x = x;
        //this.y = y;
        //this.z = 0;
    }
    
    @Override
    public String toString(){
        String valor = String.format(
                getNome()+"("+
                        String.format(""+getX()).replace(".", ",")+","+
                        String.format(""+getY()).replace(".", ",")+")"
        );
        return valor;
    }
    
}
