/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author vagner
 */
public class PoligonalFechada<T> extends Poligonal {
    private final Ponto2D[] vertices;
    private double x, y, z;

    public PoligonalFechada(T[] poligono) throws RuntimeException {
        super(poligono);
        this.vertices = (Ponto2D[]) poligono;
        int numVert = this.vertices.length;
        if(numVert<3) throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        
    }
    
    @Override
    public double getComprimento(){
        
        double resultado=0;
        int i;
        for(i=0; i<this.vertices.length-1; i++){
        //for (PontoXZ vertice : vertices) {
               double X = this.vertices[i+1].getX();
               double X1 = this.vertices[i].getX();
               double Y = this.vertices[i+1].getY();
               double Y1 = this.vertices[i].getY();
               double Z = this.vertices[i+1].getZ();
               double Z1 = this.vertices[i].getZ();

               resultado=resultado+vertices[i+1].dist(vertices[i]);
           
        }
        resultado=resultado+vertices[0].dist(vertices[i]);
        return resultado; 
    }
    
    
    
}
