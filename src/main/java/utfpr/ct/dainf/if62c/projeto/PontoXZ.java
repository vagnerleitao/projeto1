/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author vagner
 */
public class PontoXZ extends Ponto2D {
    private double x, y, z;
    
    public PontoXZ(){
        setX(0.0);
        setY(0.0);
        setZ(0.0);
    }
    
    public PontoXZ(double x, double z){
        setX(x);
        setY(0.0);
        setZ(z);
        //this.x = x;
        //this.y = 0;
        //this.z = z;
        //Ponto valor = new Ponto(this.x,this.y,this.z);
/*        double X = valor.getX();
        double Y = valor.getY();
        double Z = valor.getZ();
        System.out.printf("X = %f, Y=%f, Z=%f\n", X, Y, Z); */
    }
    
   
    @Override
    public String toString(){
        String valor = String.format(
                getNome()+"("+
                        String.format(""+getX()).replace(".", ",")+","+
                        String.format(""+getZ()).replace(".", ",")+")"
        );
        return valor;
    }    
    
    
}
