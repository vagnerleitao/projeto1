/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author vagner
 */
public class Poligonal<T> extends Ponto2D {
    private final Ponto2D[] vertices;
    private double x, y, z;
    
    public Poligonal(T[] poligono) throws RuntimeException {
        this.vertices = (Ponto2D[])poligono;
        int numVert = this.vertices.length;
        //System.out.println("Poligonal"+numVert);
        if(numVert<3) throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        //else {
        //    double x1 = vertices[0].getX();
        //    System.out.printf("Poligonal OK - x1 = %f\n",x1);
        //}
    }

           
    public int getN(){
        return this.vertices.length;
    }
    
    public T get(int v){
        if(v >= getN() || v < 0) return null;
        T retorna_ponto = (T)this.vertices[v];
        //double valor = this.vertices[v].getX();
        //System.out.println("valor x = "+valor);
        return retorna_ponto;
    
    }
    
    public void set(int v, T valorvertice){
        if( v < getN() && v >= 0 ){
            Ponto2D valor = (Ponto2D)valorvertice;
            //double x2 = vertices[0].getX();
            //double x3 = valor.getZ();
            this.vertices[v]=(Ponto2D)valor;
            //System.out.println("Tamanho vertices = "+vertices.length
            //+" - Valor Vertice = "+valor+" Indice Vertice = "+v);
            //PontoXZ[] insere_ponto = new PontoXZ[v];
            //insere_ponto[v] = (PontoXZ)valorvertice;
        }
    }
    
    public double getComprimento(){
        
        double resultado=0;
        for(int i=0; i<vertices.length-1; i++){
        //for (PontoXZ vertice : vertices) {
               double X = this.vertices[i+1].getX();
               double X1 = this.vertices[i].getX();
               double Y = this.vertices[i+1].getY();
               double Y1 = this.vertices[i].getY();
               double Z = this.vertices[i+1].getZ();
               double Z1 = this.vertices[i].getZ();

               resultado=resultado+vertices[i+1].dist(vertices[i]);
           
        }
        
        return resultado; 
    }
    
    
}
